/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.lib;

/**
 * Direction enum class 
 */
public class Direction {
	private int xDisplacement;
	private int yDisplacement;
	

	private Direction(int xMovement, int yMovement) {
		this.xDisplacement = xMovement;
		this.yDisplacement = yMovement;
	}

	public static final Direction North = new Direction(0,-1);
	public static final Direction South = new Direction(0,1);
	public static final Direction East = new Direction(1,0);
	public static final Direction West = new Direction(-1, 0);
	
	public static final Direction[] possibilities = {North,South,East,West};

	public int yMovement(Speed speed, int iterationLength) {
		return yDisplacement * speed.distanceOver(iterationLength);
	}

	public int xMovement(Speed speed, int iterationLength) {
		return xDisplacement * speed.distanceOver(iterationLength);
	};

	public int getXDisplacement() {
		return xDisplacement;
	}

	public int getYDisplacement() {
		return yDisplacement;
	}


	@Override
	public String toString() {
		if (xDisplacement < 0) return "West";
		if (xDisplacement > 0) return "East";
		if (yDisplacement < 0) return "North";
		return "South";
	}
}
