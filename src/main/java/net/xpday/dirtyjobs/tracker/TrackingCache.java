/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.tracker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import net.xpday.dirtyjobs.lib.CloseToOther;
import net.xpday.dirtyjobs.lib.Collision;
import net.xpday.dirtyjobs.lib.Direction;
import net.xpday.dirtyjobs.lib.Engine;
import net.xpday.dirtyjobs.lib.Speed;

public class TrackingCache {

	public static class Entry {

		private int timestamp;
		private int xPos;
		private int yPos;
		private Direction direction;
		private Speed speed;
		private Engine engineStatus;
		private CloseToOther closingStatus;
		private Collision collisionStatus;
		private TrackingCache cache;
		private byte identification;
		private int color = 0x0FF0000;

		public Entry(byte decoderId, TrackingCache cache) {
			this(decoderId, 0, 0, 0, Direction.North, Speed.Nul, Engine.Off,
					CloseToOther.No, Collision.No);
			this.cache = cache;
		}

		public Entry(byte identification, int timestamp, int xPos, int yPos,
				Direction direction, Speed speed, Engine engineStatus,
				CloseToOther closingStatus, Collision collisionStatus) {
			this.identification = identification;
			this.timestamp = timestamp;
			this.xPos = xPos;
			this.yPos = yPos;
			this.direction = direction;
			this.speed = speed;
			this.engineStatus = engineStatus;
			this.closingStatus = closingStatus;
			this.collisionStatus = collisionStatus;
		}

		public Entry(Entry entry) {
			this.identification = entry.identification;
			this.cache = entry.cache;
			this.timestamp = entry.timestamp;
			this.xPos = entry.xPos;
			this.yPos = entry.yPos;
			this.direction = entry.direction;
			this.speed = entry.speed;
			this.engineStatus = entry.engineStatus;
			this.closingStatus = entry.closingStatus;
			this.collisionStatus = entry.collisionStatus;
			this.color = entry.color;
		}

		public Entry(byte identification, int timestamp, int xPos, int yPos,
				Direction direction, Speed speed, Engine engineStatus,
				CloseToOther closingStatus, Collision collisionStatus, int color) {
			this(identification,timestamp,xPos,yPos,direction,speed,engineStatus,closingStatus,collisionStatus);
			this.setColor(color);
		}

		/**
		 * @return the timestamp
		 */
		public int getTimestamp() {
			return timestamp;
		}

		/**
		 * @param timestamp
		 *            the timestamp to set
		 */
		public void setTimestamp(int timestamp) {
			this.timestamp = timestamp;
		}

		/**
		 * @return the xPos
		 */
		public int getXPos() {
			return xPos;
		}

		/**
		 * @param pos
		 *            the xPos to set
		 */
		public void setXPos(int pos) {
			xPos = pos;
		}

		/**
		 * @return the yPos
		 */
		public int getYPos() {
			return yPos;
		}

		/**
		 * @param pos
		 *            the yPos to set
		 */
		public void setYPos(int pos) {
			yPos = pos;
		}

		/**
		 * @return the direction
		 */
		public Direction getDirection() {
			return direction;
		}

		/**
		 * @param direction
		 *            the direction to set
		 */
		public void setDirection(Direction direction) {
			this.direction = direction;
		}

		/**
		 * @return the speed
		 */
		public Speed getSpeed() {
			return speed;
		}

		/**
		 * @param speed
		 *            the speed to set
		 */
		public void setSpeed(Speed speed) {
			this.speed = speed;
		}

		/**
		 * @return the engineStatus
		 */
		public Engine getEngineStatus() {
			return engineStatus;
		}

		/**
		 * @param engineStatus
		 *            the engineStatus to set
		 */
		public void setEngineStatus(Engine engineStatus) {
			this.engineStatus = engineStatus;
		}

		/**
		 * @return the closingStatus
		 */
		public CloseToOther getClosingStatus() {
			return closingStatus;
		}

		/**
		 * @param closingStatus
		 *            the closingStatus to set
		 */
		public void setClosingStatus(CloseToOther closingStatus) {
			this.closingStatus = closingStatus;
		}

		/**
		 * @return the collisionStatus
		 */
		public Collision getCollisionStatus() {
			return collisionStatus;
		}

		/**
		 * @param collisionStatus
		 *            the collisionStatus to set
		 */
		public void setCollisionStatus(Collision collisionStatus) {
			this.collisionStatus = collisionStatus;
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof Entry))
				return false;
			Entry other = (Entry) obj;
			return identification == other.identification
					&& timestamp == other.timestamp && xPos == other.xPos
					&& yPos == other.yPos && direction == other.direction
					&& speed.equals(other.speed)
					&& engineStatus == other.engineStatus
					&& closingStatus == other.closingStatus
					&& collisionStatus == other.collisionStatus
					&& color == other.color;
		}

		@Override
		public String toString() {
			return String.format("CacheEntry(0x%06X, " + identification + ", " + timestamp + ", "
					+ xPos + ", " + yPos + ", " + direction + ", " + speed
					+ ", " + engineStatus + ", " + closingStatus + ", "
					+ collisionStatus + ")", color);
		}

		public void commit() {
			cache.add(new Entry(this));

		}

		public byte getIdentification() {
			return identification;
		}

		public void setColor(int color) {
			this.color = color;
		}

		public int getColor() {
			return color;
		}
	}

	private static TrackingCache _instance = new TrackingCache();

	public static TrackingCache getInstance() {
		return _instance;
	}

	private HashMap<Byte, ArrayList<Entry>> entries = new HashMap<Byte, ArrayList<Entry>>();
	private HashMap<Byte, Entry> tops = new HashMap<Byte, Entry>();
	private BlockingQueue<ArrayList<Entry>> displayQueue = new ArrayBlockingQueue<ArrayList<Entry>>(
			10);

	public void add(Entry entry) {
		Byte identification = Byte.valueOf(entry.getIdentification());
		synchronized (entries) {
			if (!entries.containsKey(identification))
				entries.put(identification, new ArrayList<Entry>());
			entries.get(identification).add(entry);
			tops.put(identification, entry);
			entries.notify();
		}

	}

	public Entry lastEntry(byte decoderId) {
		Byte identification = Byte.valueOf(decoderId);
		synchronized (entries) {
			if (!entries.containsKey(identification))
				return null;
			return entries.get(identification).get(
					entries.get(identification).size() - 1);
		}
	}

	public Entry createCacheEntry(byte decoderId) {
		return new Entry(decoderId, this);
	}

	public Entry entry(byte decoderId, int i) {
		synchronized (entries) {
			Byte identification = Byte.valueOf(decoderId);
			if (!entries.containsKey(identification))
				return null;
			if (entries.get(identification).size() <= i)
				return null;
			return entries.get(identification).get(i);
		}
	}

	public void clear() {
		synchronized (entries) {
			entries.clear();
			tops.clear();
		}
	}

	public ArrayList<Entry> top(){
		synchronized (entries) {
			return new ArrayList<Entry>(tops.values());
		}
	}

	public int size(byte identification) {
		synchronized (entries) {
			return entries.containsKey(Byte.valueOf(identification)) ? entries.get(Byte.valueOf(identification)).size() : 0;
		}
	}
}
