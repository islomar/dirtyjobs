/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.tracker;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import net.xpday.dirtyjobs.interfaces.TrackableField;
import net.xpday.dirtyjobs.lib.Protocol;

public class VehicleTrackingService implements Runnable {

	private static VehicleTrackingService _instance;
	private Thread thread1;
	private Thread thread2;
	private Thread watchDogThread;
	private boolean stopRequested;
	private Object started1 = new Object();
	private Object watchDog = new Object();
	private final ByteBuffer buffer = ByteBuffer.allocate(16384);

	protected int port;
	protected String host;
	protected ArrayList<MessageDecoder> decoders = new ArrayList<MessageDecoder>();

	public static VehicleTrackingService getInstance() {
		return _instance;
	}

	public static void initialize(String host, int port)
			throws InterruptedException {
		_instance = new VehicleTrackingService(host, port);
	}

	protected VehicleTrackingService(String host, int port)
			throws InterruptedException {
		this.host = host;
		this.port = port;
		start();
	}

	protected void start() throws InterruptedException {
		stopRequested = false;
		thread1 = new Thread(this);
		thread2 = new Thread(new Runnable() {
			public void run() {
				for (;;) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
					}
					synchronized (watchDog) {
						watchDog.notify();
					}
				}
			}

		});
		watchDogThread = new Thread(new Runnable() {
			public void run() {
				for (;;) {
					try {
						synchronized (watchDog) {
							watchDog.wait();
						}
					} catch (InterruptedException e) {
					}
					MessageDecoder[] handlers;
					synchronized (decoders) {
						handlers = new MessageDecoder[decoders.size()];
						handlers = decoders.toArray(handlers);
					}
					for (MessageDecoder decoder : handlers) {
						decoder.tick();
					}

				}
			}
		});
		watchDogThread.start();
		thread2.start();
		thread1.start();
		synchronized (started1) {
			started1.wait();
		}
	}

	public void stop() throws InterruptedException {
		stopRequested = true;
		if (thread1 != null) {
			thread1.join(500);
		}
	}

	public void run() {
		try {
			ServerSocketChannel ssc = ServerSocketChannel.open();

			ssc.configureBlocking(false);
			ServerSocket ss = ssc.socket();
			InetSocketAddress isa = new InetSocketAddress(port);
			ss.bind(isa);

			Selector selector = Selector.open();

			ssc.register(selector, SelectionKey.OP_ACCEPT);
			synchronized (started1) {
				started1.notifyAll();
			}
			while (!stopRequested) {
				int num = selector.select();

				if (num == 0) {
					continue;
				}

				Set<SelectionKey> keys = selector.selectedKeys();
				Iterator<SelectionKey> it = keys.iterator();
				while (it.hasNext()) {
					SelectionKey key = it.next();
					if ((key.readyOps() & SelectionKey.OP_ACCEPT) == SelectionKey.OP_ACCEPT) {
						Socket s = ss.accept();
						SocketChannel sc = s.getChannel();
						sc.configureBlocking(false);
						sc.register(selector, SelectionKey.OP_READ);
					} else if ((key.readyOps() & SelectionKey.OP_READ) == SelectionKey.OP_READ) {
						SocketChannel sc = null;
						try {
							sc = (SocketChannel) key.channel();
							boolean ok = processInput(sc);
							if (!ok) {
								key.cancel();
								Socket s = null;
								try {
									s = sc.socket();
									s.close();
								} catch (IOException ie) {
									System.err.println("Error closing socket "
											+ s + ": " + ie);
								}
							}
						} catch (IOException ie) {
							key.cancel();
							try {
								sc.close();
							} catch (IOException ie2) {
								System.out.println(ie2);
							}
							System.out.println("Closed " + sc);
						}
					}
				}
				keys.clear();
			}
			selector.close();
			ss.close();
		} catch (IOException ie) {
			System.err.println(ie);
		}

	}

	private boolean processInput(SocketChannel sc) throws IOException {
		byte[] frameBuffer = new byte[Protocol.FullFrameLength];
		buffer.clear();
		sc.read(buffer);
		buffer.flip();
		// If no data, close the connection
		if (buffer.limit() == 0) {
			return false;
		}
		while (buffer.remaining() > 0) {
			int length = buffer.get(buffer.position()
					+ Protocol.FrameLengthIndex);
			buffer.get(frameBuffer, 0, length);

			processMessage(frameBuffer, length);
		}
		return true;
	}

	protected void processMessage(byte[] frameBuffer, int length) {
		MessageDecoder[] handlers;
		synchronized (decoders) {
			handlers = new MessageDecoder[decoders.size()];
			handlers = decoders.toArray(handlers);
		}
		for (MessageDecoder decoder : handlers) {
			decoder.handleMessage(frameBuffer[Protocol.IdentificationIndex],
					frameBuffer, length);
		}
	};

	public Integer trackVehicle(String vehicleHost, MessageDecoder decoder)
			throws RemoteException, NotBoundException, MalformedURLException {
		String url = "//" + vehicleHost + "/TrackableField";
		TrackableField stub = (TrackableField) Naming.lookup(url);
		Integer result = stub
				.trackVehicle(host, port, decoder.identification());
		synchronized (decoders) {
			decoders.add(decoder);
		}
		return result;
	}

	public void removeTrackers() {
		synchronized (decoders) {
			decoders.clear();
		}
	}

	protected static void initialize(VehicleTrackingService instance) {
		_instance = instance;
	}
}