/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.tracker;

public interface MessageDecoder {

	void handleMessage(byte identification, byte[] message, int length);

	byte identification();

	void tick();

}
