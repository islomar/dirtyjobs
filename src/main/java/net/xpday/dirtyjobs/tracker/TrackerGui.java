/**
 * Dirty Jobs workshop code
 *  
 * (c) 2008 Westgeest Consultancy, Living Software B.V., Piecemeal Growth (Qwan) 
 */
package net.xpday.dirtyjobs.tracker;

import java.awt.Graphics;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JFrame;
import javax.swing.JPanel;

import net.xpday.dirtyjobs.lib.AbstractVehicleFieldPanel;

class VehicleTrackingPanel extends AbstractVehicleFieldPanel {
	private TrackingCache trackingCache;

	public VehicleTrackingPanel() {
		super();
		trackingCache = TrackingCache.getInstance();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for(TrackingCache.Entry entry : trackingCache.top()) {
			canvas(g).fillSquare(entry.getXPos(), entry.getYPos(), 10, entry.getColor());
		}
	}
}

public class TrackerGui {

	public static void main(String[] args) {
		
		try {
			if(args.length < 3) {
				System.out.println("usage:\tTrackerGui <listenHost> <listenPort> <vehicleHost>");
				System.exit(1);
			}
			String listenHost = args[0];
			int listenPort = Integer.parseInt(args[1]);
			String vehicleHost = args[2];
			System.out.println("             will start server on: " + listenHost + ":" + listenPort);
			System.out.println("will connect to vehicle field on : " + vehicleHost);
			VehicleTrackingService.initialize(listenHost, listenPort);
			VehicleTrackingPanel vehicelTrackingPanel = new VehicleTrackingPanel();
			
			VehicleMessageDecoder[] decoders = new VehicleMessageDecoder[]{
					new VehicleMessageDecoder(vehicleHost, (byte)0x01),
					new VehicleMessageDecoder(vehicleHost, (byte)0x02),
					new VehicleMessageDecoder(vehicleHost, (byte)0x03)
			};
			
			showFrame(vehicelTrackingPanel, 800, 600);

			for (;;) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				vehicelTrackingPanel.redraw();
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	private static void showFrame(JPanel vehicelFieldPanel,
			int width, int height) {
		JFrame f = new JFrame("Tracker Field");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.add(vehicelFieldPanel);
		f.setSize(width, height);
		f.setVisible(true);
	}

}
